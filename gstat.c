#include "utils_serveur.h"

int shm_id;
int sem_id;
memoire_partagee* mem;
progs program;

void afficherStat(int num){
	down();
	program = mem->tab_progs[num];
	up();
	printf("%d\n",program.num_prog);
	printf("%s\n",program.name);
	printf("%d\n",program.compile);
	printf("%d\n",program.execution);
	printf("%d\n",program.time_ex);
}

int main(int argc, char const *argv[])
{
	int index_number=0;
	if(argc>1){
		index_number=atoi(argv[1]);
	}else{
		printf("pas de numero introduit en argument \n");
	}
	init_shm();
	init_sem(1);
	shm_id=get_shm();
	mem=get_memo();
	sem_id=get_sem();

	afficherStat(index_number);

	/* code */
	del_sem();
	sshmdt();
	return 0;
}
