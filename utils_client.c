#include "utils_client.h"

bool contFils = true;

//Méthode de thread
void handler_recurate(){
  message tab_prog_exc[1000];
  int nbProg = 0;
  close(pipefd[1]);
  while(contFils){
    read(pipefd[0], &code, sizeof(int));
    if(code == 1){ //ajout au tab
      message msg;
      msg.code = -1;
      read(pipefd[0], &code, sizeof(int));
      msg.num_prog_name = code;
      tab_prog_exc[nbProg] = msg;
      nbProg++;
    }
    else if (code == 0) { //Beat
      connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));
      ret = write(sockfd, &tab_prog_exc, nbProg*sizeof(message));
      checkNeg(ret, "erreur d'ecriture");
      close(sockfd);
    }
  }
  close(pipefd[0]);
}

void handler_beat(){
  code = 0;

	while(contFils){
		usleep(delay*1000);
		ret = write(pipefd[1], &code, 5*sizeof(char));
		checkNeg(ret, "erreur d'écriture !");
  }
  close(pipefd[1]);
}

//methode des signaux
void client_handler(int sig) {
  wait(&beat);
  wait(&recurate);
  close(pipefd[1]);
  printf("Au revoir !\n");
  exit(0);
}

void thread_handler(int sig) {
  contFils = false;
}

void addFile() {
  char readBuffer[MAX];
  message msg;
  msg.code = -1;
  printf("Veuillez entrer le chemin du fichier à envoyer au serveur.\n");
  ret = read(0, readBuffer, MAX);
  readBuffer[ret-1] = '\0';
  strcpy(msg.result, readBuffer);
  char* pnt = strrchr(readBuffer, '/');

  if(pnt != NULL){
    pnt++;
    strcpy(msg.name_prog, pnt);
    msg.num_prog_name = strlen(pnt);
  } else {
    strcpy(msg.name_prog, readBuffer);
    msg.num_prog_name = strlen(readBuffer);
  }

  //printf(" code: %d\n path: %s\n nom prog: %s\n longueur nom: %d\n",msg.code, msg.result, msg.name_prog, msg.num_prog_name);
  ret = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));
  checkNeg(ret, "Erreur de connection");
  write(sockfd, &msg, sizeof(message)); //envoie de message
  //ajout de texte
  fd = fopen(msg.result, "r");
  while(fgets(readBuffer, MAX, fd) != 0){
    write(sockfd, readBuffer, MAX);
    printf("%s", readBuffer);
  }
  fclose(fd);
  //fin envoi texte
  shutdown(sockfd, SHUT_WR); //utilisé pour fermer l'écriture (pour la lecture utiliser SHUT_RD)
  message receive;
  read(sockfd, &receive, sizeof(message));
  char readBuf[MAX];
  printf("Numero de programme: %d\n", receive.num_prog_name);
  if(receive.code == 1) {
    while((nbCharRd = read(sockfd, readBuf, MAX))) {
      printf("%s", readBuf);
    }
  }
  close(sockfd);
}

void executeFile() {
  char readBuffer[MAX];
  char readNumber[4];
  message msgExc;
  msgExc.code = -2;
  printf("Veuillez entrer le numéro du programme à executer\n\n");
  read(0, readNumber, sizeof(int));
  msgExc.num_prog_name = atoi(readNumber);
  ret = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));
  checkNeg(ret, "Erreur connection");
  write(sockfd, &msgExc, sizeof(message));
  message retour;
  read(sockfd, &retour, sizeof(message));
  printf(" n° programme: %d\n Etat: %d\n Temps d'exectution: %d\n code de fin: %d\n", retour.num_prog_name, retour.stat_prog, retour.time_exc, retour.code);
  if(retour.code == 1){
    printf("Resultat: ");
    while((nbCharRd = read(sockfd, readBuffer, MAX))){
      write(1, readBuffer, nbCharRd);
      printf("%s", readBuffer);
    }
  }
}
