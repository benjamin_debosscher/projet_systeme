#include "utils_client.h"

bool contPere = true;

void AddFileQueue() {
  code = 1;
  int num;
  write(1, "Entrer le numero de programme a ajouter a la liste: \n", 52);
  scanf("%d", &num);
  write(pipefd[1], &code, sizeof(int));
  write(pipefd[1], &num, sizeof(int));
}

int main (int argc, char* argv[]) {
  if(argc != 4) {
    perror("Nombre d'argument invalide !");
    exit(1);
  }
  delay = atoi(argv[3]);
  checkNeg(delay, "Erreur de battement !");
  port = atoi(argv[2]);

  //initialisation du handler pour les fils
  sig.sa_handler = thread_handler;
  sigaction(SIGINT,&sig, NULL);

  //initialiser le pipe et les fils
  ret = pipe(pipefd);
  checkNeg(ret, "Erreur creation de pipe");
  recurate = fork_and_run(*handler_recurate);
  close(pipefd[0]);
  beat = fork_and_run(*handler_beat);

  //initialisation du handler pour le Père
  sig.sa_handler = client_handler;
  sigaction(SIGINT,&sig, NULL);

  printf("Bonjour et bienvenue dans l'app client !\n");

  do {
    //initialisation SOCKET
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    memset (&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    inet_aton(argv[1],&addr.sin_addr);

    printf(" Entrez + pour ajouter un fichier sur le serveur\n Entrez * pour ajouter un programme a la liste d'execution\n Entrez @ pour executer un programme\n Entrez q pour quitter le programme\n");
    int fd = read(0, choice, PIPE);
    checkNeg(fd, "erreur read");
    switch(choice[0]){
      case '+':
        addFile();
        break;
      case '*':
        AddFileQueue();
        break;
      case '@':
        executeFile();
        break;
      case 'q':
        contPere = false;
        break;
      default:
        printf("Veuillez rentrer +, *, @ ou q.\n");
        break;
    }
  }while(contPere);
  kill(beat, SIGINT);
  kill(recurate, SIGINT);
  wait(&beat);
  wait(&recurate);
  close(pipefd[1]);
  printf("Au revoir !\n");
  exit(0);
}
