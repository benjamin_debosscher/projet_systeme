#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>



#define MAX 255
#define PIPE 2

void checkCond(bool cond, char* msg);

void checkNeg(int res, char* msg);

void checkNull(void* res, char* msg);

//SOCKET
/* Convert a DNS domain name to IP v4
 Necessary for socket connection because IP required */
int hostname_to_ip (char * hostname, char* ip);

//buffered read
int readline(int fd, char *line);

pid_t fork_and_run(void (*handler)());

/*
typedef struct sockaddr_in {
  sa_family_t sin_family;   //famille d'adresses : AF_INET
  uint16_t sin_port;        //Port
  struct in_adrr sin_adrr;  //Adresse internet
  char sin_zero;
} sockaddr_in;
*/
//MESSAGE
typedef struct message {
  int code;
  int num_prog_name;  //UtilisÃ© pour la longueur du nom lors d'un ajout et pour le numÃ©ro de prog lors d'une execution
  char name_prog[MAX];
  int stat_prog;
  int time_exc;
  char result[MAX];            //message d'erreur ou affichage du programme demandÃ©
} message;

#endif
