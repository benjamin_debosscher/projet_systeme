#include "utils_serveur.h"

#define EXTENTION ".c"

#define REP "code/"
#define REP2 "compile/"


#define ONE 1
#define COMPEXR ".txt"
#define EXEEXT "res_exe.txt"
#define CURENT "./"
int nb_client = 0;
int sockfd;
int sem_id;
int shm_id;
memoire_partagee* mem;

char c[MAX];
char s[MAX];
char ex[MAX];

void handler1() {
	printf(" c %s\n",c );
	
	
	printf("s %s\n",s );
  execl("/usr/bin/cc", "cc", "-o", s, c, NULL);
  perror("Error execl 1");
}

void handler2(void* arg0) {
	printf("ex %s\n",ex);
	printf("s %s\n", s);
  execl(ex, s, NULL);
  perror("Error exec 2");
}


long now() {
  struct timeval  tv;
  int res = gettimeofday(&tv, NULL);
  checkNeg(res, "Error gettimeofday");
  return tv.tv_sec * 1000000 + tv.tv_usec;
}


void fermeture(int sig){
	if(sig==SIGINT){
		del_sem();
		sshmdt();
		close(sockfd);
		exit(0);
	}
}

void gestionMessage(void* arg){

	int* sockfdp = arg;
	int  sockfd = *sockfdp;

	message msg;
	read(sockfd,&msg,sizeof(message));

	if(msg.code==-1) {

		//s'occuper de l ajout de fichier
		progs to_add;
		//récupération du numéro du prog dans la mem partagée
		down();
			to_add.num_prog=mem->taille;
			mem->taille++;
		up();
		// preparation de la struc a ajouter
		strcpy(to_add.name,msg.name_prog);
		to_add.execution=0;
		to_add.time_ex=0;

		sprintf(c,"%s%d%s",REP,to_add.num_prog,EXTENTION);
		FILE* towrite=fopen(c,"w");

		char tab [MAX];
		int resize;
		while((resize=read(sockfd,tab,MAX))!=0){


			fputs(tab,towrite);


		}
		fclose(towrite);
		sprintf(s,"%s%d%s",REP2,to_add.num_prog,COMPEXR);
		printf("s %s\n",s);
		// la compilation
		char run[MAX];
		sprintf(run,"%s%s%d%s",CURENT,REP,to_add.num_prog,COMPEXR);
		printf("run %s\n",run );
		int fd = open("run", O_CREAT | O_WRONLY| O_TRUNC, 0666);
  	checkNeg(fd, "ERROR open");
  	int stderr_copy = dup(2);
  	checkNeg(stderr_copy, "ERROR dup");
  	int ret = dup2(fd, 2);
 		checkNeg(ret, "ERROR dup2");
		//compilation du fichier

		strcpy(s, c);
		s[strlen(s)-2] = '\0';
		strcat(ex, "./");
		strcat(ex, s);
		sprintf(s,"%s%d",REP2,to_add.num_prog);
		fork_and_run(handler1);
		int status;
		wait(&status);
		printf("%s\n",s );
		printf("SI %d != 0, ALORS regarde dans res_compile.txt\n", WEXITSTATUS(status));

		ret = dup2(stderr_copy, 2);
  	checkNeg(ret, "ERROR dup");
  	close(stderr_copy);
		long t1, t2;
		//execution du FICHIER
		if (WIFEXITED(status) && !WEXITSTATUS(status)) {
			t1 = now();
			fork_and_run(handler2);
			wait(&status);
			t2 = now();
		}
		to_add.time_ex = t2-t1;

  		if(status==0){
				printf("temps d'exc %d\n", to_add.time_ex);
  			message tosend;
  			tosend.num_prog_name=to_add.num_prog;
  			tosend.code=0;
  			write(sockfd,&tosend,sizeof(message));
  		}else{
				printf("etat %d\n", status);
  			message tosend;
  			tosend.num_prog_name=to_add.num_prog;
  			to_add.compile=false;
  			tosend.code=-1;
  			write(sockfd,&tosend,sizeof(message));
  			fd = open("run",O_RDONLY,PERM);
  			char resultat[MAX];
  			int size2;
  			while((size2=read(fd,resultat,MAX))!=0){
  				write(sockfd,resultat,size2);
  			}
  			close(fd);
  		}
  		down();
  		mem->tab_progs[to_add.num_prog]=to_add;
  		mem->nb_execution++;
  		up();


	}else if(msg.code==-2){

		message for_user;
		progs to_exec;
		int num_exec=msg.num_prog_name;
		int state=0;
		down();
		if(mem->taille<num_exec)state=-2;
		else{
			to_exec=mem->tab_progs[num_exec];
		}
		up();
		if(state==-2){
			printf("inexistant\n");
			for_user.code=-2;
			for_user.num_prog_name=msg.num_prog_name;
			for_user.stat_prog=-1;
			for_user.time_exc=-1;
		}else if(to_exec.compile!=true){
			printf("compile pas\n");
			for_user.code=-1;
			for_user.num_prog_name=msg.num_prog_name;
			for_user.stat_prog=-1;
			for_user.time_exc=-1;
		}else{
			char prog [MAX];
			sprintf(prog,"%d",to_exec.num_prog);
			char com [MAX];
			sprintf(com,"%s%s%d",CURENT,REP,to_exec.num_prog);
			//redirection du stdout sur
			int fd = open(EXEEXT, O_CREAT | O_WRONLY| O_TRUNC, 0666);
  			checkNeg(fd, "ERROR open");

  			int stdout_copy = dup(1);
  			checkNeg(stdout_copy, "ERROR dup");

  			int out = dup2(fd, 1);
  			checkNeg(out, "ERROR dup1");
  			int status;
			int t1=now();
			fork_and_run(handler2);
			wait(&status);
			int t2=now();

			out = dup2(stdout_copy, 1);
 			checkNeg(out, "ERROR dup");
  		close(stdout_copy);
  		bool goodExec=false;
			int time=t2-t1;
			if(status==-1){
				printf("erreur a l'execution\n");
				for_user.code=0;
				for_user.num_prog_name=msg.num_prog_name;
				for_user.stat_prog=-1;
				for_user.time_exc=-1;
			}else{
				printf("ok tous reussi\n");
				goodExec=true;
				for_user.code=1;
				for_user.num_prog_name=msg.num_prog_name;
				for_user.stat_prog=status;
				for_user.time_exc=time;
			}
			write(sockfd,&for_user,sizeof(message));
			if (goodExec){
				char send_result[MAX];
				fd = open(EXEEXT,O_RDONLY,PERM);
				int end;
				while((end=read(fd,send_result,MAX))!=0){
					write(sockfd,send_result,end);
				}
				close(fd);
			}
			down();
				mem->tab_progs[msg.num_prog_name].execution++;
				mem->tab_progs[msg.num_prog_name].time_ex+=time;
				mem->nb_execution++;
			up();
		}
	}
	close(sockfd);
}

int main(int argc, char const *argv[])

{
	//initialisation socket + recupéré les lien du semaphore et shm
	sockfd=initSocketServer(PORT);
	init_shm();
	init_sem(1);
	//recuperer les variable dans le
	sem_id=get_sem();
	shm_id=get_shm();
	mem=get_memo();
	//lisen
	listen(sockfd,1);



	//armer le signal pour ctrl_C
	struct sigaction newact= {{0}};
	newact.sa_handler= fermeture;
	sigaction(SIGUSR1, &newact, NULL);
	//
	while(true){
		int clientFD=accept(sockfd,NULL,NULL);
		if(clientFD!=-1 && nb_client <= 49){
			nb_client++;
			printf("Client connecté !\n");
			//write(clientFD,"connecter au serveur",20);
			fork_and_run1(gestionMessage, &clientFD);
			close(clientFD);
		}
	}
	/* code */
	return 0;
}
