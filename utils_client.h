#ifndef _UTILS_CLIENT_H_
#define _UTILS_CLIENT_H_

//CONSTANTE
#include <fcntl.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "utils.h"

int delay, port, ret, sockfd, code, nbCharRd;
FILE* fd;
int pipefd[PIPE];
pid_t beat, recurate;
char choice[PIPE];
struct sockaddr_in addr;
struct sigaction sig;

//int code = 1;
//Struct

//methode threads
void handler_recurate();

void handler_beat();

//methode des signaux
void client_handler(int sig);

void thread_handler(int sig);

void addFile();

void executeFile();
#endif
