#ifndef _UTILSSERVEUR_h_
#define _UTILSSERVEUR_h_

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <time.h>
#include "utils.h"


//constante
#define SEM_KEY 369
#define SHM_KEY 042
#define PERM 0666
#define MAXSEM 50
#define PORT 8081
#define NAMEFILESIZE 256
#define MAXPROGS 1000
#define INITIALIZED 0

//Struct
union semun {
  int val;    /* Value for SETVAL */
  struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
  unsigned short  *array;  /* Array for GETALL, SETALL */
  struct seminfo  *__buf;  /* Buffer for IPC_INFO (Linux-specific) */
};

typedef struct progs{
  int num_prog;
  char name[NAMEFILESIZE] ;
  bool  compile;
  int execution;
  int time_ex;
}progs;

typedef struct memoire_partagee{
  int nb_execution;
  int taille;
  progs tab_progs[MAXPROGS] ;
} memoire_partagee;

//methode
memoire_partagee* get_memo();
//void
void create_shm();
void init_shm();
int get_shm();
void sshmdt();
void del_shm();

//semaphore
void create_sem(int val);
void init_sem(int val);
int get_sem();
void add_sem(int val);
void up();
void down();
void del_sem();

//conection;
int initSocketServer(int port);

pid_t fork_and_run1(void (*handler)(void *), void* arg0);
pid_t fork_and_run2(void (*handler)(void *, void*), void* arg0, void* arg1);
#endif
