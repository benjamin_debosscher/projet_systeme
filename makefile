CFLAGS =cc -D_DEFAULT_SOURCE -std=c11 -pedantic -Wvla -Wall -Werror

all: serveur client maint gstat


#Serveur
serveur : serveur.o utils_serveur.o utils.o
	$(CFLAGS) -o serveur serveur.o utils_serveur.o utils.o
serveur.o: serveur.c utils_serveur.h
	$(CFLAGS) -c serveur.c
maint: maint.c utils_serveur.o utils.o
	$(CFLAGS) -o maint maint.c utils_serveur.o utils.o
gstat: gstat.c utils_serveur.o utils.o
	$(CFLAGS) -o gstat gstat.c utils_serveur.o utils.o

#Client
client : client.o utils_client.o utils.o
	$(CFLAGS) -o client client.o utils_client.o utils.o
client.o: client.c utils_client.h
	$(CFLAGS) -c client.c

# utils
utils_serveur.o: utils_serveur.c utils.h
	$(CFLAGS) -c utils_serveur.c
utils_client.o : utils_client.c utils.h
	$(CFLAGS) -c utils_client.c
utils.o : utils.c utils.h
	$(CFLAGS) -c utils.c


clean :
	rm *.o
	
clean_exc :
	rm serveur
	rm client
	rm maint
	rm gstat
