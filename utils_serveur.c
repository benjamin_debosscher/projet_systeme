#include "utils_serveur.h"

    int sem_id;
    int shm_id;
    //memoire partager

    int* nbrPorgs;
    memoire_partagee* mem;

    int get_shm(){
      return shm_id;
    }

    int  get_sem(){
      return sem_id;
    }
    memoire_partagee* get_memo(){
      return mem;
    }

      void create_shm(){
      shm_id = shmget(SHM_KEY, sizeof(memoire_partagee), IPC_CREAT | PERM);
      checkNeg(shm_id, "Error shmget");
      mem = shmat(shm_id, NULL, 0);
      checkCond(mem == (void*) -1, "Error shmat");
    }

    void init_shm(){
      shm_id = shmget(SHM_KEY, sizeof(memoire_partagee),  PERM);
      checkNeg(shm_id, "Error shmget");
      mem = shmat(shm_id, NULL, 0);
      checkCond(mem == (void*) -1, "Error shmat");
    }

   void sshmdt() {
      int r = shmdt(mem);
      checkNeg(r, "Error shmdt");
    }

    void del_shm() {
      int r = shmctl(shm_id, IPC_RMID, NULL);
      checkNeg(r, "Error shmctl");
    }

    // semaphore
    void create_sem(int val){
        // CREATE A SET OF ONE SEMAPHORE.
      // THE NUMBER ASSOCIATED WITH THIS SEMAPHORE IS 0.
      sem_id = semget(SEM_KEY, 1, IPC_CREAT | PERM);
      checkNeg(sem_id, "Error semget");

      // INIT THE SEMAPHORE VALUE TO val
      union semun arg;
      arg.val = val;

      int rv = semctl(sem_id, 0, SETVAL, arg);
      checkNeg(rv, "Error semctl");
    }
    void init_sem(int val) {

      // CREATE A SET OF ONE SEMAPHORE.
      // THE NUMBER ASSOCIATED WITH THIS SEMAPHORE IS 0.
      sem_id = semget(SEM_KEY, 1,  PERM);
      checkNeg(sem_id, "Error semget");

      // INIT THE SEMAPHORE VALUE TO val
      union semun arg;
      arg.val = val;

      int rv = semctl(sem_id, 0, SETVAL, arg);
      checkNeg(rv, "Error semctl");
    }

    void add_sem(int val) {
      struct sembuf sem;
      sem.sem_num = 0;
      sem.sem_op = val;
      sem.sem_flg = 0;

      int rc = semop(sem_id, &sem, 1);
      checkNeg(rc, "Error semop");
    }

    void down() {
      add_sem(-1);
    }

    void up() {
      add_sem(1);
    }

    void del_sem() {
      int rv = semctl(sem_id, 0, IPC_RMID);
      checkNeg(rv, "Error semctl");
    }
    //init connexion serveur
    int initSocketServer(int port)
    {
      int sockfd;
      struct sockaddr_in addr;
      int ret;
      /* AF_INET -> internet comunication , AF_UNIX -> local communication */
      // SOCK_STREAM -> TCP
      sockfd = socket(AF_INET, SOCK_STREAM, 0);
      checkNeg(sockfd,"socket server creation error");

      /* no socket error */
      memset(&addr,0,sizeof(addr)); /* en System V */
      addr.sin_family = AF_INET;
      addr.sin_port = htons(port);
      addr.sin_addr.s_addr = htonl(INADDR_ANY);
      ret = bind(sockfd,(struct sockaddr *)&addr,sizeof(addr));
      checkNeg(ret,"server bind error");

      /* no bind error */
      ret = listen(sockfd,5);
      /* no listen error */
      return sockfd;
    }

    pid_t fork_and_run1(void (*handler)(void *), void* arg0) {
      int childId = fork();
      checkNeg(childId, "Error [fork_and_run]");

      // child process
      if (childId == 0) {
        (*handler)(arg0);
        exit(EXIT_SUCCESS);
      }

      return childId;
    }

    pid_t fork_and_run2(void (*handler)(void*, void*), void* arg0, void* arg1) {
      int childId = fork();
      checkNeg(childId, "Error [fork_and_run]");

      // child process
      if (childId == 0) {
        (*handler)(arg0, arg1);
        exit(EXIT_SUCCESS);
      }

      return childId;
    }
    //accept request

    //close connexion serveur
