#include <stdbool.h>
 #include <fcntl.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <time.h>
 #include <unistd.h>

 #include <sys/time.h>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/wait.h>

 #include "utils_serveur.h"

    int sem_id;
    int shm_id;
    int* nbrProgs;
    /*memoire_partagee* mem;
    		void init_shm(){
		        shm_id = shmget(SHM_KEY, sizeof(memoire_partagee), IPC_CREAT | PERM)
		        checkNeg(shm_id, "Error shmget");
		        mem = shmat(shm_id, NULL, 0);
		        checkCond(mem == (void*) -1, "Error shmat");
		    }
		     void del_shm() {
		        int r = shmctl(shm_id, IPC_RMID, NULL);
		        checkNeg(r, "Error shmctl");
		    }


             void init_sem(int val) {

              // CREATE A SET OF ONE SEMAPHORE.
              // THE NUMBER ASSOCIATED WITH THIS SEMAPHORE IS 0.
              sem_id = semget(SEM_KEY, 1, IPC_CREAT | PERM);
              checkNeg(sem_id, "Error semget");

              // INIT THE SEMAPHORE VALUE TO val
              union semun arg;
              arg.val = val;

              int rv = semctl(sem_id, 0, SETVAL, arg);
              checkNeg(rv, "Error semctl");
            }

             void del_sem() {
              int rv = semctl(sem_id, 0, IPC_RMID);
              checkNeg(rv, "Error semctl");
            }
            void add_sem(int val) {
              struct sembuf sem;
              sem.sem_num = 0;
              sem.sem_op = val;
              sem.sem_flg = 0;

              int rc = semop(sem_id, &sem, 1);
              checkNeg(rc, "Error semop");
            }

            void down() {
              add_sem(-1);
            }

            void up() {
              add_sem(1);
            }

*/
int main(int argc, char const *argv[])
{


	if(argc==1){
		printf("pas d'argument en parametre ./maint.c type [opt] \n");
		exit(1);
	}
	int toswitch=atoi(argv[1]);
  int occupation;
	switch(toswitch){
		// initialise les semaphore et memoire partagée
		case 1:
			create_sem(1);
			create_shm();
		break;
		// detruit les semaphore et memoire partagée
		case 2:
			init_sem(1);
			init_shm();
			del_sem();
			del_shm();
		break;
		case 3:
			if(argv[2]==NULL){
				printf("pas de optiont de temps fins du programe \n");
				exit(1);
			}else{
				init_sem(1);
				occupation=atoi(argv[2]);
				down();
				usleep(occupation*1000);
				up();
			}
		break;
	}

	/* code */
	return 0;
}
