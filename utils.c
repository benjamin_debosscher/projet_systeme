#include "utils.h"

void checkCond(bool cond, char* msg) {
  if (cond) {
    perror(msg);
    exit(EXIT_FAILURE);
  }
}

void checkNeg(int res, char* msg) {
  checkCond(res < 0, msg);
}

void checkNull(void* res, char* msg) {
  checkCond(res == NULL, msg);
}


pid_t fork_and_run(void (*handler)()){
	  int childId = fork();
	  checkNeg(childId, "Error fork()");

	  if (childId == 0) {  // child process
	    (*handler)();
	    exit(EXIT_SUCCESS);
	  }
	  return childId;
}
